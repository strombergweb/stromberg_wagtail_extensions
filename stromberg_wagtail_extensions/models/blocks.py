from django import forms
from django.utils.encoding import force_str
from wagtail.blocks import StreamBlock, CharBlock, RichTextBlock, StructBlock, FieldBlock, TextBlock, \
    RawHTMLBlock, ChoiceBlock, PageChooserBlock, URLBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.images.blocks import ImageChooserBlock

from stromberg_wagtail_extensions.models.icon_block import IconBlock


class IntegerBlock(FieldBlock):
    def __init__(self, required=True, help_text=None, max_value=None, min_value=None, **kwargs):
        # CharField's 'label' and 'initial' parameters are not exposed, as Block handles that functionality natively
        # (via 'label' and 'default')
        self.field = forms.IntegerField(
            required=required,
            help_text=help_text,
            max_value=max_value,
            min_value=min_value
        )
        super(IntegerBlock, self).__init__(**kwargs)

    def get_searchable_content(self, value):
        return [force_str(value)]


class BadgeBlock(StructBlock):
    icon = IconBlock()
    heading = CharBlock(max_length=20)
    text = CharBlock(max_length=100)
    link = PageChooserBlock(required=False)


class IconLinkBlock(StructBlock):
    icon = IconBlock()
    text = CharBlock(max_length=20)
    link = PageChooserBlock()


class ImageFormatChoiceBlock(FieldBlock):
    field = forms.ChoiceField(choices=(
        ('left', 'Wrap left'), ('right', 'Wrap right'), ('mid', 'Mid width'), ('full', 'Full width'),
    ))


class ImageBlock(StructBlock):
    image = ImageChooserBlock()
    caption = RichTextBlock(required=False)
    alignment = ImageFormatChoiceBlock()
    width = IntegerBlock(required=False, default=300, min_value=1, max_value=2048)

    @property
    def is_full(self):
        return self.alignment == "full"


class CADDownloadableBlock(StructBlock):
    name = CharBlock(required=False)
    description = CharBlock()
    jpg = ImageChooserBlock(label="JPG File", required=False)
    pdf = DocumentChooserBlock(label="PDF File", required=False)
    dwg = DocumentChooserBlock(label="DWG File", required=False)

    class Meta:
        icon = "doc-full-inverse"


class FeatureBlock(StructBlock):
    icon = IconBlock(required=False)
    text = CharBlock()

    class Meta:
        icon = "pick"


class FeatureListBlock(StreamBlock):
    feature = FeatureBlock()

    class Meta:
        icon = "list-ul"


class FeatureTableBlock(StructBlock):
    heading = CharBlock(required=True)
    image = ImageChooserBlock(required=False, label="Background Image")
    subheading = CharBlock(required=False)
    features = FeatureListBlock(required=True)

    class Meta:
        icon = "form"


class FeatureTableRowBlock(StreamBlock):
    feature_table = FeatureTableBlock()


class CaptionedThumbnailBlock(StructBlock):
    title = CharBlock()
    image = ImageChooserBlock()
    description = TextBlock(required=False)

    class Meta:
        icon = 'image'


class CaptionedThumbnailsRowBlock(StreamBlock):
    captioned_thumbnail = CaptionedThumbnailBlock()

    class Meta:
        icon = 'image'


class AdvantageBoxColorChoiceBlock(FieldBlock):
    field = forms.ChoiceField(choices=(
        ('box-primary', 'Primary Color'), ('box-success', 'Green'), ('box-warning', 'Yellow'), ('box-danger', 'Red'), ('box-royal', 'Purple'),
    ))


class AdvantageBoxBlock(StructBlock):
    color = AdvantageBoxColorChoiceBlock()
    heading = CharBlock(max_length=50)
    body = RichTextBlock()
    image = ImageChooserBlock()

    class Meta:
        icon = 'form'


class AdvantageBoxRowBlock(StreamBlock):
    advantage_box = AdvantageBoxBlock()


class HTMLAlignmentChoiceBlock(FieldBlock):
    field = forms.ChoiceField(choices=(
        ('normal', 'Normal'), ('full', 'Full width'),
    ))


class AlignedHTMLBlock(StructBlock):
    html = RawHTMLBlock()
    alignment = HTMLAlignmentChoiceBlock()

    class Meta:
        icon = "code"


class PullQuoteBlock(StructBlock):
    quote = TextBlock("quote title")
    attribution = CharBlock()

    class Meta:
        icon = "openquote"


class StreamTablesDownloadPageBlock(StreamBlock):
    new_table_heading = CharBlock(label="New Table Heading", icon="title", classname="title")
    table_row = CADDownloadableBlock(label="Add Drawing Row")


class DefinitionBlock(StructBlock):
    term = CharBlock(help_text='A question or term.')
    answer = RichTextBlock(help_text='The answer or the definition.')

    class Meta:
        icon = 'plus'


class DefintionsListBlock(StreamBlock):
    definition = DefinitionBlock()


class DefintionsListSectionBlock(StructBlock):
    heading = CharBlock(required=False, help_text="Section heading like 'FAQ' or 'Glossary'")
    definitions = DefintionsListBlock()

    class Meta:
        icon = 'list-ul'


class ThumbnailedDocumentBlock(StructBlock):
    thumbnail = ImageChooserBlock(required=False)
    document = DocumentChooserBlock()
    caption = CharBlock(required=False)


class StreamFieldBlock(StreamBlock):
    h2 = CharBlock(icon="title", classname="title", label="H2 with Separator")
    h3 = CharBlock(icon="title", classname="title")
    paragraph = RichTextBlock(icon="pilcrow")
    aligned_image = ImageBlock(label="Aligned image", icon="image")
    pullquote = PullQuoteBlock()
    aligned_html = AlignedHTMLBlock(icon="code", label='Raw HTML')
    document = ThumbnailedDocumentBlock(icon="doc-full-inverse")
    feature_table_row = FeatureTableRowBlock()
    captioned_thumbnail_row = CaptionedThumbnailsRowBlock()
    advantage_box_row = AdvantageBoxRowBlock(label="Advantages Color Boxes")
    collapsable_definitions = DefintionsListSectionBlock(label="Definitions / FAQ")
