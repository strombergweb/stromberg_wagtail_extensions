from django.db import models
from wagtail.admin.panels import FieldPanel, PageChooserPanel
from wagtail import models as core_models
from wagtail.documents import models as doc_models


# A couple of abstract classes that contain commonly used fields
class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        core_models.Page,
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.CASCADE
    )
    link_document = models.ForeignKey(
        doc_models.Document,
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.CASCADE,
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
        FieldPanel('link_document'),
    ]

    class Meta:
        abstract = True
