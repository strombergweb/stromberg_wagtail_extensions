from django.apps import AppConfig


class StrombergWagtailExtensionsConfig(AppConfig):
    name = 'stromberg_wagtail_extensions'
    verbose_name = 'Stromberg Wagtail Extensions'
    default_auto_field = 'django.db.models.AutoField'
