from setuptools import setup
from stromberg_wagtail_extensions.__version__ import __version__

setup(
    name='stromberg_wagtail_extensions',
    version=__version__,
    packages=[
        'stromberg_wagtail_extensions',
        'stromberg_wagtail_extensions.models'
    ],
    install_requires=[
        'Django>=5.0.0',
        'wagtail>=5.0.0',
    ],
    # https://pypi.org/classifiers/
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: Other/Proprietary License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    url='https://bitbucket.org/strombergweb/stromberg_wagtail_extensions.git',
    license='None',
    author='Phillip Stromberg',
    author_email='phillip@4stromberg.com',
    description='Extensions for wagtail to help make it easier to develop'
)
